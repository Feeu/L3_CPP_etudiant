#include "Doubler.hpp"
#include "Client.hpp"
#include "Produit.hpp"
#include "Magasin.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupClient) { };

TEST(GroupClient, Client_test1)  {
    Client c(42,"toto");
    CHECK_EQUAL(c.getId(),42);
    CHECK_EQUAL(c.getNom(),"toto");
}

TEST_GROUP(GroupProduit) {};

TEST(GroupProduit, Produit_test1) {
    Produit p(3,"super produit");
    CHECK_EQUAL(p.getId(),3);
    CHECK_EQUAL(p.getDescription(), "super produit");
}

TEST_GROUP(GroupMagasin) {};

TEST(GroupMagasin, Magasin_test1) {
    /** TESTS SUR LES CLIENTS **/
    Magasin m;
    m.ajouterClient("toto");
    m.ajouterClient("titi");
    m.ajouterClient("tata");
    CHECK_EQUAL(m.nbClients(),3);
    m.supprimerClient(0);
    CHECK_EQUAL(m.nbClients(),2);
    m.ajouterClient("tutu");
    CHECK_EQUAL(m.nbClients(),3);

    // Suppression d'un client qui n'existe pas
    CHECK_THROWS(std::string,m.supprimerClient(10));
}

TEST(GroupMagasin, Magasin_test2) {
    /** TESTS SUR LES PRODUITS **/
    Magasin m;
    m.ajouterProduit("produit 1");
    m.ajouterProduit("produit 2");
    m.ajouterProduit("protuit 3");


    // Suppression d'un client qui n'existe pas
    CHECK_THROWS(std::string,m.supprimerProduit(10));
}

TEST(GroupMagasin, Magasin_test3) {
    /** TESTS SUR LA FIN D'IMPLEMENTATION **/
    Magasin m;
    m.ajouterClient("toto");
    m.ajouterClient("titi");
    m.ajouterProduit("produit 1");
    m.ajouterProduit("produit 2");
    std::vector<int> clientsLibres = m.calculerClientsLibres();
    std::vector<int> produitsLibres = m.calculerProduitsLibres();
    LONGS_EQUAL(produitsLibres.size(),2);
    LONGS_EQUAL(clientsLibres.size(),2);

    m.ajouterLocation(0,1);
    // Suppression de locations qui n'existent pas
    CHECK_THROWS(std::string, m.supprimerLocation(0,0));
    CHECK_THROWS(std::string, m.supprimerLocation(1,0));
    CHECK_THROWS(std::string, m.supprimerLocation(1,1));

    // Ajout d'une location qui existe deja
    CHECK_THROWS(std::string,m.ajouterLocation(0,1));


    clientsLibres = m.calculerClientsLibres();
    LONGS_EQUAL(clientsLibres.size(),1);

    produitsLibres = m.calculerProduitsLibres();
    LONGS_EQUAL(produitsLibres.size(),1);
}



