#include "Magasin.hpp"
#include <iostream> // DEBUG
Magasin::Magasin() : _idCourantClient(0), _idCourantProduit(0)
{
    IDCLIENT = 0;
    IDPRODUIT = 0;
}

int Magasin::nbClients() const {
    return _clients.size();
}

void Magasin::ajouterClient(const std::string &nom) {
    Client c(IDCLIENT++,nom);
    _clients.push_back(c);
}

void Magasin::afficherClients() const {
    for(const Client &c : _clients) c.afficherClient();
}

void Magasin::supprimerClient(int idClient) {
    auto dernierClient = _clients.end();
    --dernierClient;

    for(auto &c: _clients) if(c.getId() == idClient) {std::swap(c,*dernierClient);_clients.pop_back();return;}
    throw std::string("erreur: ce client n'existe pas");
}

void Magasin::supprimerProduit(int idProduit) {
    auto dernierProduit = _produits.end();
    --dernierProduit;

    for(auto &p: _produits) if(p.getId() == idProduit) {std::swap(p,*dernierProduit);_produits.pop_back();return;}
    throw std::string("erreur: ce produit n'existe pas");
}

void Magasin::ajouterProduit(const std::string &description) {
    Produit p(IDPRODUIT++,description);
    _produits.push_back(p);
}

int Magasin::nbLocations() const {
    return _locations.size();
}

void Magasin::ajouterLocation(int idClient, int idProduit) {
    for(const Location &l : _locations)
        if(l._idClient==idClient and l._idProduit==idProduit)
            throw std::string("erreur : la location existe deja");
    Location l {idClient,idProduit};
    _locations.push_back(l);
}

void Magasin::afficherLocations() const {
    for(const Location &l : _locations) l.afficherLocation();
}

void Magasin::supprimerLocation(int idClient, int idProduit) {
    auto derniereLocation = _locations.end();
    --derniereLocation;

    for(Location &l: _locations) {
        if(l._idClient == idClient and l._idProduit == idProduit) {
            std::swap(l,*derniereLocation);
            _locations.pop_back();
            return;
        }
    }
    throw std::string("erreur: cette location n'existe pas");
}

bool Magasin::trouverClientDansLocation(int idClient) const {
    for(const Location &l: _locations)
        if(l._idClient==idClient) return true;
    return false;
}

std::vector<int> Magasin::calculerClientsLibres() const {
    std::vector<int> clientsLibres;
    for(const Client &c : _clients)
        if(not trouverClientDansLocation(c.getId()))
            clientsLibres.push_back(c.getId());
    return clientsLibres;
}

bool Magasin::trouverProduitDansLocation(int idProduit) const {
    for(const Produit &p: _produits)
        if(p.getId()==idProduit) return true;
    return false;
}

std::vector<int> Magasin::calculerProduitsLibres() const {
    std::vector<int> produitsLibres;
    for(const Produit &p : _produits)
        if(not trouverProduitDansLocation(p.getId()))
            produitsLibres.push_back(p.getId());
    return produitsLibres;
}
