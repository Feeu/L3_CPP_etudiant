#include "Doubler.hpp"
#include "Location.hpp"
#include "Client.hpp"
#include "Produit.hpp"
#include "Magasin.hpp"
#include <iostream>

int main() {
    Magasin m;

    m.ajouterClient("toto");
    m.ajouterClient("titi");
    m.ajouterProduit("p1");
    m.ajouterProduit("p2");
    m.ajouterLocation(0,1);
    std::vector<int> clientsLibres = m.calculerClientsLibres();
    std::vector<int> produitsLibres = m.calculerProduitsLibres();


    std::cout << clientsLibres.size() << std::endl;
    return 0;
}

