#include "Doubler.hpp"

#include <iostream>

int main() {
    int a;
    a = 42;
    int *p;
    p = &a;
    *p = 37;

    int* t;
    t = new int[10];
    t[2]=42;
    delete[] t;
    t = nullptr;
    std::cout << doubler(21) << std::endl;
    std::cout << doubler(21.f) << std::endl;
    return 0;
}

