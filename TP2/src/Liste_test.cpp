#include "liste.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupListe) { };

TEST(GroupListe, Liste_test1)  {
    Liste liste;
    CHECK_EQUAL(liste.getTaille(),0);
    liste.ajouterDevant(2);
    CHECK_EQUAL(liste.getTaille(),1);
    liste.ajouterDevant(3);
    CHECK_EQUAL(liste.getTaille(),2);
    liste.ajouterDevant(4);
    CHECK_EQUAL(liste.getTaille(),3);

    CHECK_EQUAL(liste.getElement(0),4);
    CHECK_EQUAL(liste.getElement(1),3);
    CHECK_EQUAL(liste.getElement(2),2);
    CHECK_EQUAL(liste.getElement(5),0);
    CHECK_EQUAL(liste.getElement(-10),0);
}
