#ifndef LISTE_H
#define LISTE_H

struct Noeud {
    int _valeur;
    Noeud *_suivant;
};

struct Liste {
    Noeud *_tete;

    Liste();
    ~Liste();
    void ajouterDevant(int);
    int getTaille();
    int getElement(int);
};

#endif // LISTE_H
