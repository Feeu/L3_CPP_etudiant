#include "liste.hpp"

Liste::Liste() {
    _tete = nullptr;
}


Liste::~Liste() {
    Noeud *courant = _tete;
    while(courant) {
        _tete = courant->_suivant;
        delete courant;
        courant = _tete;
    }
}

void Liste::ajouterDevant(int valeur) {
    _tete = new Noeud {valeur, _tete};
}

int Liste::getTaille() {
    Noeud *courant = _tete;
    int c = 0;
    while(courant) {
        c++;
        courant = courant->_suivant;
    }
    return c;
}


/*int Liste::getElement(int indice) {
    if(indice<0) return 0;
    Noeud *courant = _tete;
    for(int i=0;i<indice;i++)
        if(courant->_suivant) courant = courant->_suivant;
        else return 0;

    if(courant) return courant->_valeur;
    else return 0;
}*/

int Liste::getElement(int indice) {
    if(indice<0) return 0;
    Noeud *courant = _tete;
    for(int i=0;i<indice and courant;i++) courant = courant->_suivant;
    if(not courant) return 0;
    return courant->_valeur;
}
