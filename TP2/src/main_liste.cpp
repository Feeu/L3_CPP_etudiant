#include "liste.hpp"
#include <iostream>

int main() {
    Liste liste;
    liste.ajouterDevant(13);
    liste.ajouterDevant(37);

    for(int i=0;i<2;i++) std::cout << liste.getElement(i) << " ";
    std::cout << std::endl;
    return 0;
}
