cmake_minimum_required( VERSION 3.0 )
project( TP_5 )
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -Wall -Wextra" )

find_package( PkgConfig REQUIRED )
pkg_check_modules( PKG_CPPUTEST REQUIRED cpputest )
pkg_check_modules(GTK REQUIRED gtkmm-3.0 )
pkg_check_modules(GLIB REQUIRED glibmm-2.4)
include_directories( ${PKG_CPPUTEST_INCLUDE_DIRS} )
include_directories(${GTK_INCLUDE_DIRS})
include_directories(${GLIB_INCLUDE_DIRS})

# programme principal
add_executable( main.out src/main.cpp
    src/FigureGeometrique.cpp
    src/Ligne.cpp
    src/PolygoneRegulier.cpp)
target_link_libraries( main.out ${GTK_LIBRARIES} ${GLIB_LIBRARIES})

# programme de test
add_executable( main_test.out src/main_test.cpp
    src/Geometrie_test.cpp
    src/FigureGeometrique.cpp
    src/Ligne.cpp
    src/PolygoneRegulier.cpp)


add_executable( main_gtk.out src/main_gtk.cpp
    src/FigureGeometrique.cpp
    src/Ligne.cpp
    src/PolygoneRegulier.cpp
    src/ViewerFigures.cpp
    src/ZoneDessin.cpp)
target_link_libraries(main_gtk.out ${GTK_LIBRARIES} ${GLIB_LIBRARIES})
target_link_libraries( main_test.out ${GTK_LIBRARIES} ${GLIB_LIBRARIES} ${PKG_CPPUTEST_LIBRARIES} )
