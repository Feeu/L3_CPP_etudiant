#ifndef LIGNE_H
#define LIGNE_H

#include "FigureGeometrique.hpp"
#include "Point.hpp"
#include "Couleur.hpp"

class Ligne : public FigureGeometrique
{
private:
    Point _p0,_p1;

public:
    Ligne(const Couleur &,const Point &,const Point &);
    void afficher(const Cairo::RefPtr<Cairo::Context>&) const;
    const Point & getP0() const;
    const Point & getP1() const;

};

#endif // LIGNE_H
