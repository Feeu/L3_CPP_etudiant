#include "PolygoneRegulier.hpp"
#include <cmath>
#include <iostream>

PolygoneRegulier::PolygoneRegulier(const Couleur &couleur, const Point &centre,
                                   int rayon, int nbCotes)
    : FigureGeometrique(couleur),_nbPoints(nbCotes)
{
    float pas = 2*M_PI/_nbPoints;
    float angle = 0;
    for(int j=0;j<nbCotes;j++) {
        _points.push_back(Point {(int) ( rayon*cos(angle) + centre._x ),
                      (int) ( rayon*sin(angle) + centre._y )});
        angle += pas;
    }
}

void PolygoneRegulier::afficher(const Cairo::RefPtr<Cairo::Context> &cr) const {
    cr->set_source_rgb(_couleur._r,_couleur._g,_couleur._b);
    cr->move_to(_points[0]._x,_points[0]._y);
    for(int i=0;i<_points.size();i++) {
        cr->line_to(_points[i]._x,_points[i]._y);
    }
    cr->line_to(_points[0]._x,_points[0]._y);
    cr->stroke();
    std::cout << "PolygoneRegulier " << _couleur._r << "_" << _couleur._g << "_" << _couleur._b;
    for(int i=0;i<_nbPoints;i++) std::cout << " " << _points[i]._x << "_" << _points[i]._y;
    std::cout << std::endl;
}

int PolygoneRegulier::getNbPoints() const {
    return _nbPoints;
}

const Point &PolygoneRegulier::getPoint(int indice) const {
    if(indice < 0 or indice >= _nbPoints) throw std::string("Erreur : indice incorrect !");
    return _points[indice];
}

PolygoneRegulier::~PolygoneRegulier() {

}
