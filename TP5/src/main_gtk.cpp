#include <gtkmm.h>

#include "ViewerFigures.hpp"

int main(int argc, char ** argv) {
    /*
    Gtk::Main kit(argc, argv); // application gtkmm
    Gtk::Window window; // fenetre principale


    window.show_all();
    kit.run(window); // lance la boucle evenementielle return 0;*/
    ViewerFigures vf(argc,argv);
    vf.run();
}
