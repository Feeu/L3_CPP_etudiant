
#include "ViewerFigures.hpp"

ViewerFigures::ViewerFigures(int argc, char** argv)
    :_kit(argc,argv),_dessin()
{
    _window.set_title("ViewerFigures");
    _window.set_default_size(640,480);
    _window.add(_dessin);

}

void ViewerFigures::run(){
    _window.show_all();
    _kit.run(_window);
}
