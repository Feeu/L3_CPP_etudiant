#include <iostream>
#include "Fibonacci.hpp"
#include "Vecteur3.hpp"

int main() {
  std::cout << "Hello World !" << std::endl;
  std::cout << fibonacciIteratif(7) << std::endl;
  std::cout << fibonacciRecursif(7) << std::endl;
  Vecteur vecteur {2,3,6};
  afficher(vecteur);
  vecteur.afficher();
  std::cout << vecteur.norme() << std::endl;
  return 0;
}
