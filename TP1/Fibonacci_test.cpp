#include <CppUTest/CommandLineTestRunner.h>
#include "Fibonacci.hpp"

TEST_GROUP(GroupFibIt){ };
TEST_GROUP(GroupFibRec){ };

TEST(GroupFibIt,test_fibIt_1){
  int result = fibonacciIteratif(1);
  CHECK_EQUAL(1, result);
}
TEST(GroupFibIt,test_fibIt_2){
  int result = fibonacciIteratif(2);
  CHECK_EQUAL(1, result);
}
TEST(GroupFibIt,test_fibIt_3){
  int result = fibonacciIteratif(3);
  CHECK_EQUAL(2, result);
}
TEST(GroupFibIt,test_fibIt_4){
  int result = fibonacciIteratif(4);
  CHECK_EQUAL(3, result);
}
TEST(GroupFibIt,test_fibIt_5){
  int result = fibonacciIteratif(5);
  CHECK_EQUAL(5, result);
}

TEST(GroupFibRec,test_fibRec_1){
  int result = fibonacciRecursif(1);
  CHECK_EQUAL(1, result);
}
TEST(GroupFibRec,test_fibRec_2){
  int result = fibonacciRecursif(2);
  CHECK_EQUAL(1, result);
}
TEST(GroupFibRec,test_fibRec_3){
  int result = fibonacciRecursif(3);
  CHECK_EQUAL(2, result);
}
TEST(GroupFibRec,test_fibRec_4){
  int result = fibonacciRecursif(4);
  CHECK_EQUAL(3, result);
}
TEST(GroupFibRec,test_fibRec_5){
  int result = fibonacciRecursif(5);
  CHECK_EQUAL(5, result);
}
