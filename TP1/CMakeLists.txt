cmake_minimum_required(VERSION 3.0)
project(TP1)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -Wall -Wextra")

add_executable(programme main.cpp Fibonacci.cpp Vecteur3.cpp)

find_package(PkgConfig REQUIRED)
pkg_check_modules( PKG_CPPUTEST REQUIRED cpputest)
include_directories( ${PKG_CPPUTEST_INCLUDE_DIRS})

add_executable(main_test main_test.cpp Fibonacci.cpp Fibonacci_test.cpp Vecteur3.cpp vecteur_test.cpp)
target_link_libraries( main_test ${PKG_CPPUTEST_LIBRARIES})
