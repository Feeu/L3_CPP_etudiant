#include <CppUTest/CommandLineTestRunner.h>
#include "Vecteur3.hpp"

TEST_GROUP(GroupVecteur){ };

TEST(GroupVecteur,test_vect_1){
    Vecteur vecteur {1,4,3};
    int result = vecteur.norme();
    CHECK_EQUAL(5, result);
}

TEST(GroupVecteur,test_vect_2){
    Vecteur vecteur {2,2,1};
    int result = vecteur.norme();
    CHECK_EQUAL(3, result);
}
TEST(GroupVecteur,test_vect_3){
    Vecteur a {2,2,1};
    Vecteur b {3,2,1};
    Vecteur result = addition(a,b);
    CHECK_EQUAL(5, result.x);
    CHECK_EQUAL(4, result.y);
    CHECK_EQUAL(2, result.z);
}
TEST(GroupVecteur,test_vect_4){
    Vecteur a {2,2,1};
    Vecteur b {3,2,1};
    float result = produitScalaire(a,b);
    CHECK_EQUAL(11, result);
}
