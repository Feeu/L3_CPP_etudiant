int fibonacciIteratif(int n) {
  int n0 = 1, n1 = 1;
  if(n==1 or n==2) return 1;
  for(int i=2;i<n;i+=2) {
    n0 = n0 + n1;
    n1 = n0 + n1;
  }
  
  if(n%2==0) return n1;
  else return n0;
}

int fibonacciRecursif(int n) {
  if(n==2) return 1;
  if(n==1) return 1;
  else return fibonacciRecursif(n-2)+fibonacciRecursif(n-1);
}
