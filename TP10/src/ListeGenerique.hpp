#ifndef LISTEGENERIQUE_HPP
#define LISTEGENERIQUE_HPP

#include <cassert>
#include <ostream>

template <typename T>
// liste d'entiers avec itérateur
class ListeGenerique {
    private:
        struct Noeud {
            T _valeur;
            Noeud* _ptrNoeudSuivant;
        };
        Noeud* _ptrTete;
    public:
        class iterator {
            private:
                Noeud* _ptrNoeudCourant;
            public:
                iterator(Noeud* ptrNoeudCourant): _ptrNoeudCourant(ptrNoeudCourant){

                }

                const iterator & operator++() {
                    if(not _ptrNoeudCourant) throw std::string("ERREUR ///// Itérateur non défini ...");
                    _ptrNoeudCourant = _ptrNoeudCourant->_ptrNoeudSuivant;
                    return *this;
                }

                T& operator*() const {
                    if(not _ptrNoeudCourant) throw std::string("ERREUR ///// Itérateur non défini ...");
                    return _ptrNoeudCourant->_valeur;
                }

                bool operator!=(const iterator &it) const {
                    return  _ptrNoeudCourant != it._ptrNoeudCourant;
                }

                friend ListeGenerique;
        };

    public:
        ListeGenerique() {
            _ptrTete = nullptr;
        }


        void push_front(T a) {
            _ptrTete = new Noeud{a,_ptrTete};
        }

        T& front() const {
            if(not _ptrTete) throw std::string("ERREUR ///// Noeud non défini ...");
            return _ptrTete->_valeur;
        }

        void clear() {
            while(_ptrTete){
                Noeud* tmp = _ptrTete;
                _ptrTete = _ptrTete->_ptrNoeudSuivant;
                delete tmp;
            }
        }

        bool empty() const {
            return not _ptrTete;
        }

        iterator begin() const {
            return iterator(_ptrTete);
        }

        iterator end() const {
           return iterator(nullptr);
        }

        ~ListeGenerique() {
            clear();
        }
};
template<typename T>
std::ostream& operator<<(std::ostream& os, const ListeGenerique<T>& l) {
    for(T i: l) os << i << " ";
    return os;
}

struct Personne{
    std::string _nom;
    int _age;
};
std::ostream& operator<<(std::ostream& os, const Personne& p) {
    os << p._nom << " " << p._age;
    return os;
}
#endif // LISTEGENERIQUE_HPP
