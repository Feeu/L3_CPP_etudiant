
#ifndef LISTE_HPP_
#define LISTE_HPP_

#include <cassert>
#include <ostream>

// liste d'entiers avec itérateur
class Liste {
    private:
        struct Noeud {
            int _valeur;
            Noeud* _ptrNoeudSuivant;
        };
        Noeud* _ptrTete;
    public:
        class iterator {
            private:
                Noeud* _ptrNoeudCourant;
            public:
                iterator(Noeud* ptrNoeudCourant): _ptrNoeudCourant(ptrNoeudCourant){

                }

                const iterator & operator++() {
                    _ptrNoeudCourant = _ptrNoeudCourant->_ptrNoeudSuivant;
                    return *this;
                }

                int& operator*() const {
                    return this->_ptrNoeudCourant->_valeur;
                }

                bool operator!=(const iterator &it) const {
                    return  this->_ptrNoeudCourant != it._ptrNoeudCourant;
                }

                friend Liste; 
        };

    public:
        Liste() {
            _ptrTete = nullptr;
        }


        void push_front(int a) {
            _ptrTete = new Noeud{a,_ptrTete};
        }

        int& front() const {
            return _ptrTete->_valeur;
        }

        void clear() {
            while(_ptrTete){
                Noeud* tmp = _ptrTete;
                _ptrTete = _ptrTete->_ptrNoeudSuivant;
                delete tmp;
            }
        }

        bool empty() const {
            return not _ptrTete;
        }

        iterator begin() const {
            return iterator(_ptrTete);
        }

        iterator end() const {
           return iterator(nullptr);
        }

        ~Liste() {
            clear();
        }
};

std::ostream& operator<<(std::ostream& os, const Liste& l) {
    for(int i: l) os << i << " ";
    return os;
}

#endif

