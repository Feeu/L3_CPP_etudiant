#include <iostream>

#include "ListeGenerique.hpp"

int main() {

    ListeGenerique<int> l1;
    l1.push_front(37);
    l1.push_front(13);
    std::cout << l1 << std::endl;

    ListeGenerique<float> l2;
    l2.push_front(1.25);
    l2.push_front(3.56);
    std::cout << l2 << std::endl;


    ListeGenerique<Personne> l3;
    l3.push_front(Personne{"a",1});
    l3.push_front(Personne{"b",2});
    std::cout << l3 << std::endl;
     return 0;
}

