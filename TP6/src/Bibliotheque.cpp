#include "Bibliotheque.hpp"
#include <iostream>
#include <algorithm>
#include <sstream>
#include <fstream>
#include <istream>
Bibliotheque::Bibliotheque()
{

}

void Bibliotheque::afficher() const{
    std::cout << "Bibliotheque :" << std::endl;
    for(Livre x:*this) std::cout << x << std::endl;
}

void Bibliotheque::trierParAuteurEtTitre() {
    std::sort(begin(),end(),[](const Livre& a, const Livre& b){
        return a<b;
    });
}

void Bibliotheque::trierParAnnee() {
    std::sort(begin(),end(),[](const Livre& a, const Livre& b){
        return a.getAnnee()<b.getAnnee();
    });
}

void Bibliotheque::ecrireFichier(const std::string &nomFichier) const {

}
void Bibliotheque::lireFichier(const std::string &nomFichier){
    std::ifstream ifs(nomFichier);
    if(not ifs.is_open()) throw std::string("erreur : lecture du fichier impossible");
    Livre l;
    while(ifs >> l && !ifs.eof()) {
        push_back(l);
    }
}
