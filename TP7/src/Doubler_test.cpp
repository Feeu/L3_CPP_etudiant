#include "Image.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupImage) { };

TEST(GroupImage, Image_test1)  {
    Image I(10,10);
    CHECK_EQUAL(I.hauteur(), 10);
    CHECK_EQUAL(I.largeur(), 10);
    I.pixel(9,9) = 24;
    CHECK_EQUAL(I.pixel(9,9), 24);
}

