#ifndef IMAGE_HPP
#define IMAGE_HPP

#include <string>

class Image
{
private:
    int _largeur,_hauteur;
    int* _pixels;
public:
    Image(int largeur,int hauteur);
    Image(const Image &);
//    int getLargeur() const;
//    int getHauteur() const;
//    int getPixel(int i,int j) const;
//    void setPixel(int i,int j,int couleur);
    int largeur() const;
    int hauteur() const;
    int pixel(int i,int j) const;
    int & pixel(int i,int j);
    ~Image();
     Image & operator=(const Image &);
};

void ecrirePnm(const Image &img, const std::string &nomFichier);
void remplir(Image &img);
Image bordure(const Image &img, int couleur, int epaisseur);
#endif // IMAGE_HPP
