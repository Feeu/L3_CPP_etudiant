#include "Image.hpp"
#include <fstream>
#include <cmath>

Image::Image(int largeur,int hauteur):
    _largeur(largeur), _hauteur(hauteur), _pixels(new int[hauteur*largeur]) {

}

//int Image::getLargeur() const{
//    return _largeur;
//}

//int Image::getHauteur() const{
//    return _hauteur;
//}

//int Image::getPixel(int i, int j) const{
//    return _pixels[i*_largeur+j];
//}

//void Image::setPixel(int i, int j, int couleur){
//    _pixels[i*_largeur+j] = couleur;
//}

int Image::largeur() const{
    return _largeur;
}
int Image::hauteur() const{
    return _hauteur;
}
int Image::pixel(int i,int j) const{
    return _pixels[i*_largeur+j];
}
int & Image::pixel(int i,int j){
    return _pixels[i*_largeur+j];
}

Image::~Image() {
    delete [] _pixels;
}

void ecrirePnm(const Image &img, const std::string &nomFichier) {
    std::ofstream ofs(nomFichier);
    if(not ofs.is_open()) throw std::string("Erreur lors de l'ouverture du fichier.");
    ofs << "P2 " << img.largeur() << " " << img.hauteur()
        <<" 255";
    for(int i=0;i<img.hauteur();i++) {
        ofs << std::endl;
        for(int j=0;j<img.largeur();j++)
            ofs << img.pixel(i,j) << " ";
    }
    ofs.close();
}

void remplir(Image &img) {
    float pas = 0;
    for(int i=0;i<img.largeur();i++) {
        float tmp = (cos(pas)+1)/2;
        int val = (int) 255*tmp;
        for(int j=0;j<img.hauteur();j++) img.pixel(j,i) = val;
        pas += 0.02;
    }
}

Image bordure(const Image &img, int couleur, int epaisseur) {
    Image ret(img);
    for(int i=0;i<epaisseur;i++) {
        for(int j=0;j<img.largeur();j++) {
            ret.pixel(i,j) = couleur;
            ret.pixel(img.hauteur()-i-1,j) = couleur;
        }
        for(int j=0;j<img.hauteur();j++) {
            ret.pixel(j,i) = couleur;
            ret.pixel(j,ret.largeur()-i-1) = couleur;
        }
    }
//    for(int i=img.largeur()-epaisseur;i<img.largeur();i++)
//        for(int j=0;j<img.largeur();j++) {
//            ret.pixel(i,j) = couleur;
//            ret.pixel(j,i) = couleur;
//        }

    return ret;
}

Image::Image(const Image &i):
    _largeur(i.largeur()), _hauteur(i.hauteur()),
    _pixels(new int[_largeur*_hauteur]){
    for(int j=0;j<_largeur*_hauteur;j++) _pixels[j] = i._pixels[j];
}

Image &Image::operator=(const Image &img) {
    if(this==&img) return *this;
    _largeur = img._largeur;
    _hauteur = img._hauteur;
    delete [] _pixels;
    _pixels = new int[_largeur*_hauteur];
    for(int i=0;i<_largeur*_hauteur;i++)
        _pixels[i] = img._pixels[i];
    return *this;
}
