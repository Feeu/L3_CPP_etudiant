#include "FigureGeometrique.hpp"

FigureGeometrique::FigureGeometrique(const Couleur & c) : _couleur(c)
{

}

const Couleur & FigureGeometrique::getCouleur() const{
    return _couleur;
}

FigureGeometrique::~FigureGeometrique() {

}

