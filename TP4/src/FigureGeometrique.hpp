#ifndef FIGUREGEOMETRIQUE_HPP
#define FIGUREGEOMETRIQUE_HPP

#include "Couleur.hpp"

class FigureGeometrique
{
protected:
    Couleur _couleur;
public:
    FigureGeometrique(const Couleur &);
    const Couleur & getCouleur() const;
    virtual void afficher() const = 0;
    virtual ~FigureGeometrique();
};

#endif // FIGUREGEOMETRIQUE_HPP
