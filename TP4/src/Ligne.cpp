#include <iostream>
#include "Ligne.hpp"
#include "FigureGeometrique.hpp"
#include "Couleur.hpp"
#include "Point.hpp"

Ligne::Ligne(const Couleur & c,const Point & p0,const Point & p1) : FigureGeometrique(c), _p0(p0), _p1(p1)
{
}

const Point & Ligne::getP0() const{
    return _p0;
}

const Point & Ligne::getP1() const{
    return _p1;
}

void Ligne::afficher() const{
    std::cout << "Ligne " << _couleur._r << "_" << _couleur._g << "_" << _couleur._b
              << " " << _p0._x << "_" << _p0._y
              << " " << _p1._x << "_" << _p1._y << std::endl;
}

