#include "Stock.hpp"
#include "Inventaire.hpp"
#include <utility>
void Stock::recalculerStock(const Inventaire &inventaire) {
    for(const Bouteille &b: inventaire._bouteilles) {
        try {
            _produits.at(b._nom);
            _produits[b._nom] += b._volume;
        }
        catch(const std::exception &e) {
            _produits[b._nom]=b._volume;
        }
    }


}

std::ostream& operator <<(std::ostream& os, const Stock& i) {
    for(std::pair<std::string,float> p: i._produits)
        os << p.first << " " << p.second << std::endl;
    return os;
}

float Stock::getStock(const std::string &bouteille) const {
    try {
        _produits.at(bouteille);
        return _produits[bouteille];
    }
    catch(const std::exception &e) {
        return 0.0;
    }
}
