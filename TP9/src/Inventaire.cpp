#include "Inventaire.hpp"
#include <algorithm>

std::ostream& operator <<(std::ostream& os, const Inventaire& i){
    //for(const Bouteille &b :i._bouteilles) os << b;
    std::for_each(i._bouteilles.begin(),i._bouteilles.end(), [&os] (const Bouteille &b) {
        os << b;
    });
    return os;
}

std::istream& operator >>(std::istream& is, Inventaire& i){
    Bouteille b;
    while(is >> b){
        i._bouteilles.push_back(b);
    }
    return is;
}

void Inventaire::trier() {
    _bouteilles.sort([] (const Bouteille &a, const Bouteille &b) {
        return a._nom < b._nom;
    });
}
