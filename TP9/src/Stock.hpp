#ifndef STOCK_HPP
#define STOCK_HPP
#include <map>
#include "Inventaire.hpp"
class Stock
{
private:
std::map<std::string,float> _produits;
public:
    void recalculerStock(const Inventaire &inventaire);
    float getStock(const std::string &bouteille) const;
    friend std::ostream& operator <<(std::ostream& os, const Stock& i);
};

std::ostream& operator <<(std::ostream& os, const Stock& i);
#endif // STOCK_HPP
