#include "Stock.hpp"
#include "Inventaire.hpp"
#include <sstream>

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupStock) { };

TEST(GroupStock, TestStock_1)
{
    std::locale vieuxLoc = std::locale::global(std::locale("fr_FR.UTF-8"));
    Inventaire inventaire;
    inventaire._bouteilles.push_back(Bouteille{"boisson", "2013-06-18", 0.1});
    inventaire._bouteilles.push_back(Bouteille{"mescaline", "2013-06-18", 0.1});
    inventaire._bouteilles.push_back(Bouteille{"mescaline", "2013-06-18", 0.1});
    inventaire._bouteilles.push_back(Bouteille{"mescaline", "2013-06-18", 0.1});
    inventaire._bouteilles.push_back(Bouteille{"cyanure", "2013-08-18", 0.25});
    std::ostringstream oss;
    Stock s;
    CHECK_EQUAL(s.getStock("mescaline"),0.3);
    CHECK_EQUAL(s.getStock("cocacola"),0.0);


    std::locale::global(vieuxLoc);
}
